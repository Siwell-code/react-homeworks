import React from 'react';
import "./header.css";
import { Image } from "../Image/Image";
import logo from '../../assets/images/Logo.svg';
import { Cart } from "./Cart/cart";
import { List }  from "../List/list";
import { navigator } from "../../constants";

export class Header extends React.Component {
    render() {
        return (
        <header className="header container__position">
            <div className="headerContainer container__items">
                <Image src={logo} alt="Logo" className="logoImage"/>
                <div className="headerNavigation">
                    <List items={navigator} className="navigationList" liName="listItem"/>
                    <Cart itemsCount={this.props.productsInCart} />
                </div>               
            </div>
        </header>
    );
}

}

