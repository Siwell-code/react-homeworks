import React from "react";
import "./cart.css";
import { ReactComponent as CartIcon } from "../../../assets/images/Busket.svg";

export class Cart extends React.Component {
    constructor(props) {
      super(props);
      this.state = {
        itemsCount: this.props.initialItemsCount || 0
      };
    }
  render() {
    return (
      <div className="Cart" data-items={this.props.itemsCount }>
        <CartIcon />
      </div>
    );
  }
}



