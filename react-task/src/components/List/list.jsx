import React from 'react';
import "./list.css";
import { v4 as uuidv4 } from 'uuid';

export class List extends React.Component {
    render() {
        const { items, className, liName, blockName } = this.props;
        
        return (
            <div className={blockName}>
                <ul className={className}>
                    {items.map(item => (
                        <li key={uuidv4()} className={liName}>{item}</li>
                    ))}
                </ul>
            </div>
        );
    }
}
