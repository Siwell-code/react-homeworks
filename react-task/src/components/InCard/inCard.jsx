import React from "react";

import "./inCard.css";

export class InCard extends React.Component {
  render() {
    return (
      <input
        className="InCard"
        type="text"
        value={this.props.value}
        onChange={this.props.onChange}
      />
    );
  }
}