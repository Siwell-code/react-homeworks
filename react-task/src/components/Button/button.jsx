import React from "react";
import "./button.css";

export class Button extends React.Component {
  render() {
    return (
      <button
        className={`Button Button_${this.props.type || "primary"} Button_${
          this.props.size || "little"
        }`}
        onClick={this.props.onClick}
      >
        {this.props.text}
      </button>
    );
  }
}

