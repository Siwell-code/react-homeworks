import React from 'react';

export class Links extends React.Component {
    renderUpperCaseFirstItem(item, index) {
        if (index === 0) {
            return item.toUpperCase();
        }
        return item;
    }

    renderItem(item, index) {
        if (index === 0) {
            return <span>{this.renderUpperCaseFirstItem(item.text, index)}</span>;
        }
        return <a href={item.url} className={this.props.linkClassName}>{item.text}</a>;
    }

    render() {
        const { items, className, liClassName, blockClassName } = this.props;
        
        return (
            <div className={blockClassName}>
                <ul className={className}>
                    {items.map((item, index) => (
                        <li key={index} className={liClassName}>
                            {this.renderItem(item, index)}
                        </li>
                    ))}
                </ul>
            </div>
        );
    }
}
