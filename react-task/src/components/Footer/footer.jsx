import React from 'react';
import './footer.css';
import { Image } from "../Image/Image";
import { List } from "../List/list";
import { Links } from "./Links/links";
import logo from "../../assets/images/Logo.svg";
import {
    company,
    template,
    flowbase,
    InstagramIcon,
    TwitterIcon,
    YoutubeIcon
} from '../../constants';

export class Footer extends React.Component {
    render() {
        return (
            <footer className="footer container__position">
                <div className="footer-wrapper container__items">
                    <div className="footer-list container__items">
                        <div className="logo__container">
                            <Image src={logo} alt="Logo" className="logo__img"/>
                            <h6 className="logo__heading">
                                    Takeaway & Delivery template 
                            </h6>
                            <p className="logo__text">
                                for small - medium businesses.
                            </p>
                        </div>
                        <div className="list">
                            <List items={company} blockName="list__blocks" className="blocks__elements" liName="items__text"/>
                            <Links items={template} blockClassName="list__blocks" className="blocks__elements" liClassName="items__text"/>
                            <List items={flowbase} blockName="list__blocks" className="blocks__elements" liName="items__text"/>
                        </div>
                    </div>
                    <div className="footer__info container__items">
                        <p className="infoe__text">Built by <a href="#root">Flowbase</a> · Powered by <a href="#root">Webflow</a></p>
                        <div className="info__icons container__items">
                            <div className="container__position icons__container">
                                <InstagramIcon />
                            </div>
                            <div className="container__position icons__container">
                                <TwitterIcon />
                            </div>
                            <div className="container__position icons__container">
                                <YoutubeIcon />
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
        );
    }
}
