import React from "react";

import "./card.css";

import { Button } from "../Button/button";
import { InCard} from "../InCard/inCard";
import { Image } from "../Image/Image";

export class Card extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      itemsCount: 1,
    };
  }

  onNumberInChange = (event) => {
    if (event.target.value !== "" && Number.isNaN(+event.target.value)) {
      this.setState({ itemsCount: 1 });
    } else {
      this.setState({ itemsCount: +event.target.value });
    }
  };

  render() {
    return (
      <div className="card__block container__position">
        <div className="card__info container__position">
          <Image src={this.props.imgSrc} alt={this.props.title} className="info__image"/>
          <div className="info__description container__items">
            <div className="items__title container__items">
              <h6 className="logo__heading card__heading">{this.props.title}</h6>
              <p className="heading__price">
                $ {this.props.price} USD
              </p>
            </div>
            <p className="heading__text">
              {this.props.description}
            </p>
            <div className="info__buttons container__items">
              <InCard
                value={this.state.itemsCount}
                onChange={this.onNumberInChange}
              />
              <Button
                text="Add to cart"
                onClick={() =>
                  this.props.onAddToCart(this.props.id, this.state.itemsCount)
                }
              />
            </div>
          </div>
        </div>
      </div>
    );
  }
}


