import React from "react";

import "./CardMenu.css";

import { Button } from "../../components/Button/button";
import { Card } from "../../components/Card/card";
import { CardHeader } from "../../pages/CardHeader/CardHeader";

export class CardMenu extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      products: [],
      itemsToRender: 6,
    };
  }

  async componentDidMount() {
    const response = await fetch(
      "https://65de35f3dccfcd562f5691bb.mockapi.io/api/v1/meals"
    );
    const data = await response.json();
    this.setState({
      products: data,
    });
  }

  loadMore = () => {
    this.setState({
      itemsToRender: this.state.itemsToRender + 6,
    });
  };

  render() {
    const { products } = this.state;

    if (!products) {
      return <p>Products are loading...</p>;
    }

    const productsShown = products.slice(0, this.state.itemsToRender);

    return (
      <div className="menu-container">
        <CardHeader />
        <div className="menu-cards">
          {productsShown.map((product) => (
            <Card
              key={product.id}
              title={product.meal}
              description={product.instructions.slice(0, 100) + "...."}
              price={product.price}
              imgSrc={product.img}
              onAddToCart={this.props.onAddToCart}
            />
          ))}
        </div>
        <div className="see-more">
          {this.state.itemsToRender < products.length && (
            <Button text="See more" size="normal" onClick={this.loadMore} />
          )}
        </div>
      </div>
    );
  }
}
