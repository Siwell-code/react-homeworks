import React from "react";

import { Button } from "../../../src/components/Button/button";

export class CardHeader extends React.Component {
  render() {
    return (
        <div className="header-container">
            <h1 className="header-title">Browse our menu</h1>
            <p>Use our menu to place an order online, or <span className="phone-heading">phone<span class="tooltiptext">111-222-3333</span></span> our store to place a pickup order. Fast and fresh food.</p>
            <div className="button-block container__items">
                <Button text="Deserts" size="big" />
                <Button text="Dinner" type="secondary" size="big" />
                <Button text="Breakfast" type="secondary" size="big" />
            </div>
        </div>
    );
  }
}


