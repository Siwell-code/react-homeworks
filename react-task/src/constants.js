import { Image } from "./components/Image/Image";
import instagram from "./assets/images/Instagram.svg";
import twitter from "./assets/images/Twitter.svg";
import youtube from "./assets/images/Youtube.svg";

export const company = ["Company", "Home", "Order", "FAQ", "Contact"];

export const template = [
  { text: "Template" },
  { url: "https://www.google.com/", text: "Style Guide" },
  { url: "https://www.google.com/", text: "Changelog" },
  { url: "https://www.google.com/", text: "Licence" },
  { url: "https://www.google.com/", text: "Webflow University" },
];

export const flowbase = ["Flowbase", "More Cloneables"];

export const InstagramIcon = () => (
  <Image src={instagram} alt="Instagram" className="icons__item" />
);

export const TwitterIcon = () => (
  <Image src={twitter} alt="Twitter" className="icons__item" />
);

export const YoutubeIcon = () => (
  <Image src={youtube} alt="YouTube" className="icons__item" />
);

export const navigator = ["Home", "Menu", "Company", "Login"];
