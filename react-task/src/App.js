import React from "react";
import "./App.css";
import { Header } from "./components/Header/header";
import { Footer } from "./components/Footer/footer";
import { CardMenu } from "./pages/CardMenu/CardMenu";

class App extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      productsInCart: [],
    };
  }

  async componentDidMount() {
    const response = await fetch(
      "https://65de35f3dccfcd562f5691bb.mockapi.io/api/v1/orders"
    );
    const data = await response.json();
    this.setState({
      productsInCart: data[0].meals,
    });
  }

  onAddToCart = (id, amount) => {
    this.setState((prevState) => ({
      productsInCart: [
        ...prevState.productsInCart,
        {
          id,
          amount,
        },
      ],
    }));
  };

  render() {
    return (
      <div className="App">
        <Header
          productsInCart={this.state.productsInCart.reduce(
            (acc, current) => acc + current.amount,
            0
          )}
        />
        <div className="App__content container__position">
          <div class="background"></div>
          <CardMenu onAddToCart={this.onAddToCart} />
        </div>
        <Footer />
      </div>
    );
  }
}

export default App;
